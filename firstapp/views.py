from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
import requests
from .forms import studentRegistration
from firstapp.models import User
# Create your views here.


def index(request):
    return render(request, "index.html")


def addshow(request):
    if request.method == 'POST':
        fm = studentRegistration(request.POST)
        fm.save()

        # 2nd METHOD
        '''if fm.valid():
            nm = fm.cleaned_data['name']
            em = fm.cleaned_data['email']
            pw = fm.cleaned_data['password']
            # user modal bnaya kud mae models.py ch
            regg = User(name=nm, email=em, password=pw)
            regg.save() '''

    else:
        fm = studentRegistration()

    # sara data databse chu le k add.html page te show kr rhe

    Retrivedata = User.objects.all()

    return render(request, "add.html", {'form': fm, 'retrive': Retrivedata})


def delete(request, id):
    if request.method == 'POST':
        pi = User.objects.get(pk=id)  # pk means primary key
        pi.delete()
        return HttpResponseRedirect('/')


def update(request, id):
    if request.method == 'POST':
        pi = User.objects.get(pk=id)
        fm = studentRegistration(request.POST, instance=pi)
        fm.save()
        res = "updation is sucessfully done  "
        return render(request, "update.html", {"status": res})

        # 2nd METHOD
        ''' if fm.valid():
                nm = fm.cleaned_data['name']
                em = fm.cleaned_data['email']
                pw = fm.cleaned_data['password']
                # user modal bnaya kud mae models.py ch
                regg = User(name=nm, email=em, password=pw)
                regg.save() '''

    else:
        pi = User.objects.get(pk=id)
        fm = studentRegistration(instance=pi)

    return render(request, "update.html", {'form': fm})
